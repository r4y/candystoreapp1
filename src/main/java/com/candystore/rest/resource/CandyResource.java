package com.candystore.rest.resource;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;
import com.candystore.rest.model.Candy;
//import com.candystore.rest.utilities.ContextUtils;

@Path("/candy")
public class CandyResource {
	CandyService service = new CandyService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCandy(final @Context HttpHeaders httpHeaders) {
		int code = 200;
		List<Candy> listaRegresar = service.getAllCandies();
		rep = Response.status(code).entity(listaRegresar).build();
		/*
		 * String tmp=httpHeaders.getRequestHeader("host").get(0); Response rep;
		 * 
		 * if(ContextUtils.validateIdClaim(tmp)) { List<Candy> listaRegresar =
		 * service.getAllCandies(); rep =
		 * Response.status(code).entity(listaRegresar).build(); } else { rep =
		 * Response.status(401).entity("No autorizado").build(); }
		 */
		return rep;

	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCandy(final @PathParam("id") int id, final @Context HttpHeaders httpHeaders) {
		Candy objetoEncontrado = service.searchCandy(id);
		if (objetoEncontrado != null)
			return Response.status(200).entity(objetoEncontrado).build();
		return Response.status(404).entity("No encontrado").build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createCandy(Candy candy) {
		return Response.status(201).entity(service.addCandy(candy)).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCandy(@PathParam("id") int id, Candy candy) {
		return Response.status(202).entity(service.setCandy(id, candy)).build();
	}

}