package com.candystore.rest.service;

import com.candystore.rest.model.Candy;

public class CandyService {
	List<Candy> candyList = new ArrayList<Candy>();
	
	public Candy addCandy(Candy objCandy) {
    objCandy.setId(candyList.size());
		candyList.add(objCandy);
    return objCandy;
	}
	
	public Candy searchCandy(int id) {
		return candyList.get(id);	
	}
	
	public Candy setCandy (int id, Candy objCandy) {
		candyList.set(id, objCandy);
    return objCandy;
		
	}
	
	public List<Candy> getAllCandies()
	{
		return candyList;
	}
}
